# TestContainers

From: https://www.testcontainers.org/usage/inside_docker.html

If you don't mind downloading all of Maven central, then this works.

    docker run -it --rm -v $PWD:$PWD -w $PWD -v /var/run/docker.sock:/var/run/docker.sock maven:3 mvn test
