package javmavcontbuild;

import java.sql.SQLException;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;

public class TestPostgresContainer 
{
	@Test
	public void doTest() throws Exception
	{
		String string = new DB().startQueryStop();
		
		Assert.assertThat(string, CoreMatchers.startsWith("PostgreSQL 10"));
	}
}
