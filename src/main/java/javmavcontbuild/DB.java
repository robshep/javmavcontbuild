package javmavcontbuild;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.function.Consumer;

import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.containers.output.OutputFrame;
import org.testcontainers.containers.output.OutputFrame.OutputType;


public class DB
{
	public String startQueryStop() throws SQLException
	{
		final String imageName = "postgres:10.0";
		PostgreSQLContainer<?> pg = new PostgreSQLContainer(imageName)
				.withUsername("test").withPassword("test").withDatabaseName("test");
		pg.withLogConsumer(new LoggingConsumer());
		pg.start();
		
		Properties connectionProps = new Properties();
	    connectionProps.put("user", "test");
	    connectionProps.put("password", "test");
	    
	    Connection connection = DriverManager.getConnection("jdbc:postgresql://" 
	    								+ pg.getContainerIpAddress()
	    								+ ":"
		    							+ pg.getMappedPort(PostgreSQLContainer.POSTGRESQL_PORT)
		    							+ "/test",
	    							connectionProps);
	    
	    try(ResultSet res = connection.prepareStatement("select version();").executeQuery() )
	    {
	    	res.next();
	    	return res.getString(1);
	    }
	    finally
	    {
	    	pg.stop();
	    }
	}
	
	
	public static class LoggingConsumer implements Consumer<OutputFrame>
	{
		public void accept(OutputFrame t) {
			if(t.getType() == OutputType.STDOUT){
				System.out.println(t.getUtf8String());
			}
			else {
				System.err.println(t.getUtf8String());
			}
		}
	}
}
